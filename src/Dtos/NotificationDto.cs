
namespace GOC.BackOffice.Dtos
{
    public class NotificationDto
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Body { get; set; }
    }
}