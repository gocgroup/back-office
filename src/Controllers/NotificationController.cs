﻿using System.Threading.Tasks;
using GOC.BackOffice.Dtos;
using GOC.BackOffice.Hubs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GOC.BackOffice.Controllers
{
    //TODO protect api resource
    [Route("api/[controller]")]
    public class NotificationController : Controller
    {
        private readonly IHubContext<NotificiationHub> _hubContext;

        public NotificationController(IHubContext<NotificiationHub> hubContext)
        {
            _hubContext = hubContext;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NotificationDto notification)
        {

            //TODO implement logic to send notification to specific clients Robinson
            JObject message = JObject.FromObject(notification);
            
            await _hubContext.Clients.All.SendAsync("receiveNotification", message);

            return Ok();
        }
    }
}
