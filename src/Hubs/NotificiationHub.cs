﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace GOC.BackOffice.Hubs
{
    public class NotificiationHub : Hub
    {
        public Task Send(string data)
        {
            return Clients.All.SendAsync("Send", data);
        }
    }
}
