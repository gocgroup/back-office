import { TestBed, inject } from '@angular/core/testing';

import { SalesBarChartService } from './sales-bar-chart.service';

describe('SalesBarChartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalesBarChartService]
    });
  });

  it('should be created', inject([SalesBarChartService], (service: SalesBarChartService) => {
    expect(service).toBeTruthy();
  }));
});
