﻿import { Component, Injectable } from '@angular/core';
import { SalesBarChartService } from './sales-bar-chart.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-sales-bar-chart',
  templateUrl: './sales-bar-chart.component.html',
  styleUrls: ['./sales-bar-chart.component.css']

})
@Injectable()
export class SalesBarChartComponent {

  chart = []; // This will hold our chart info

  constructor(private _service: SalesBarChartService) {}

  ngOnInit() {
    this._service.getChartData()
      .subscribe(res => {
        //TODO replace fake data with data coming from service
        this.chart = new Chart('canvas', {
                    type: 'bar',
                    data: {
                        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
                    datasets: [{
                    label: 'Sales',
                    data: ["40000", "30000", "27000", "100000", "5000", "4000"],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function(value, index, values) {
                                return value.toLocaleString("en-US",{style:"currency", currency:"USD"});
                            }
                        }
                    }]
                }
            }
        });
      })
  }

}