import * as AppActions from '../actions/app.actions';
import { AppState } from '../interfaces/AppState';
import{ LoggedInUser} from '../interfaces/LoggedInUser'

export type Action = AppActions.AddedCompany;

const defaultState: AppState = {
  companyId: "",
  loggedInUser: {firstName:""}
}

const newState = (state, newData) => {
    return Object.assign({}, state, newData);
}

export function appReducer(state: AppState = defaultState, action: Action){
     switch (action.type) {
         case AppActions.ADDED_COMPANY:
           return newState(state, { "companyId": action.id, "loggedInUser":{"firstName":action.loggedUser} });
     }
}