import * as UserActions from '../actions/user.actions';
import { LoggedInUser } from'../interfaces/LoggedInUser'

export type Action = UserActions.AddUser;

const defaultState: LoggedInUser = {
  firstName: ""
}

const newState = (state, newData) => {
    return Object.assign({}, state, newData);
}

export function userReducer(state: LoggedInUser = defaultState, action: Action){
     switch (action.type) {
         case UserActions.ADD_USER:
           return newState(state, { "firstName": action.user });
     }
}