import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { Store} from '@ngrx/store';
import { AuthService } from '../auth/auth.service';
import { HubConnection } from '@aspnet/signalr';
import { ToastrService } from 'ngx-toastr';
import { UserNotification } from '../interfaces/UserNotification';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
@Injectable()
export class NavMenuComponent {
  private _hubConnection: HubConnection;
  public currentUser : string;
  public activateSearch: boolean;
  public notificationBadgeNumber : number;
  public notifications: Array<UserNotification> = [];

  constructor(private _authService: AuthService, private _store: Store<any>, private _toastr: ToastrService) {
    this.activateSearch = false;
    
    this._store.select('user').subscribe(userLoggedIn => {
      if(userLoggedIn != null){
        this.currentUser = userLoggedIn.firstName + '!';
      }
    });
  }

  initiateSearch() {
    this.activateSearch = true;
  }
  terminateSearch(){
    this.activateSearch = false;
  }
  

  ngOnInit() {
    this.notificationBadgeNumber = 0;
    this._hubConnection = new HubConnection('/notification');
    
    this._hubConnection.on('receiveNotification', (data: any) => {
      this.notificationBadgeNumber ++;
      const received = `Received: ${data}`;
      console.log(received);
      //this._toastr.success('Success!', received);
   });
        this._hubConnection.start()
            .then(() => {
                console.log('Hub connection started')
            })
            .catch(err => {
                console.log('Error while establishing connection')
            });
  } 

  logout(){
    this._authService.startSignoutMainWindow();
    console.log("logout call");

  }
}
