import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../services/company.service';
import { Injectable } from '@angular/core';
import { Company } from '../interfaces/Company'

@Injectable()
@Component({
  selector: 'app-company-card',
  templateUrl: './company-card.component.html',
  styleUrls: ['./company-card.component.css']
})
export class CompanyCardComponent implements OnInit {

  public company: Company;
  companyName : string;
  constructor(private _companyService: CompanyService) {
     _companyService.getCompanyById().subscribe(res=>
      {
        this.company = <Company>res.json();
        this.companyName = this.company.name;
        console.log("this comes from db");
        console.log(this.company);
      });
   }
 
  ngOnInit() {
  }

}
