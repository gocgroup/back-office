import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth-signout-callback',
  templateUrl: './auth-signout-callback.component.html',
  styleUrls: ['./auth-signout-callback.component.css']
})
export class AuthSignoutCallbackComponent implements OnInit {

  constructor(private _authService: AuthService, private _router: Router) { }

  ngOnInit() {
    this._authService.endSignoutMainWindow();

    console.log("singout complete ");
    this._authService.startAuthentication();
  }

}
