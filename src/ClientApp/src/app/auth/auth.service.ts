﻿import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { UserManager, User } from 'oidc-client';
import { environment } from '../../environments/environment';
import { Store } from '@ngrx/store';
import{ LoggedInUser } from '../interfaces/LoggedInUser';
import{ AppState } from '../interfaces/AppState';
import{ Company } from '../interfaces/Company';

import * as UserActions from '../actions/user.actions';
import * as AppActions from '../actions/app.actions';


const settings: any = {
  authority: environment.identity.authority,
  client_id: environment.identity.clientId,
  redirect_uri: environment.identity.redirectUri,
  post_logout_redirect_uri: environment.identity.logoutRedirectUri,
  response_type: environment.identity.responseType,
  scope: environment.identity.scope,

  //silent_redirect_uri: 'http://localhost:5004/silent-renew.html',
  automaticSilentRenew: true,
  accessTokenExpiringNotificationTime: 4,
  // silentRequestTimeout:10000,

  filterProtocolClaims: true,
  loadUserInfo: true
};


@Injectable()
export class AuthService {
  mgr: UserManager = new UserManager(settings);
  userLoadededEvent: EventEmitter<User> = new EventEmitter<User>();
  currentUser: User;
  loggedIn = false;
  authHeaders: Headers;

  company : Company;
  constructor(private http: Http, private store: Store<AppState>) {
    this.mgr.getUser()
      .then((user) => {
        if (user) {
          this.loggedIn = true;
          this.currentUser = user;
          this.userLoadededEvent.emit(user);
        }
        else {
          this.loggedIn = false;
        }
      })
      .catch((err) => {
        this.loggedIn = false;
      });

    this.mgr.events.addUserLoaded((user) => {
      this.currentUser = user;
      this.loggedIn = !(user === undefined);
      // if (!environment.production) {
      //   console.log('authService addUserLoaded', user);
      // }

    });

    this.mgr.events.addUserUnloaded((e) => {
      if (!environment.production) {
        console.log('user unloaded');
      }
      this.loggedIn = false;
    });

  }

 
  isLoggedIn(): boolean {
    return this.currentUser != null && !this.currentUser.expired;
  }

  getClaims(): any {
    return this.currentUser.profile;
  }

  getAuthorizationHeaderValue(): string {
    return `${this.currentUser.token_type} ${this.currentUser.access_token}`;
  }

  startAuthentication(): Promise<void> {
    debugger;
    console.log(settings);
    console.log(this.mgr);
    return this.mgr.signinRedirect();
 }

 completeAuthentication(): Promise<void> {
    return this.mgr.signinRedirectCallback().then(user => {
        console.log("completed authentication");
        this.store.dispatch( new UserActions.AddUser(user.profile.firstName));
      //   var company = {
      //     Name : "GOC",
      //     UserId : 123,
      //     PhoneNumber : "2252332234",
      //     Address : 
      //      {
      //          AddressLine1 : "123 Elm Street St",
      //          City: "Los Angeles",
      //          State: "California",
      //          ZipCode: "34322"
      //      }
      //  };
      //   this.AuthPost("http://vagrant:5001/api/companies/", company)
      //   .map(result => result).subscribe(res=>{
      //     this.company = <Company>res.json();
      //     this.store.dispatch( new AppActions.AddedCompany(this.company.id, this.company.name, "Robinson"));
      //   });
        this.currentUser = user;
    });
}
startSignout(): Promise<void> {
  return this.mgr.signoutRedirect();
}
completeSignout(): Promise<void>{
  return this.mgr.signoutRedirectCallback().then(user => {
    console.log("completed signout");
    this.mgr.clearStaleState();
   });
}


 isLoggedInObs(): Observable<boolean> {
    return Observable.fromPromise(this.mgr.getUser()).map<User, boolean>((user) => {
      if (user) {
        return true;
      } else {
        return false;
      }
    });
  }
  clearState() {
    this.mgr.clearStaleState().then(function () {
      console.log('clearStateState success');
    }).catch(function (e) {
      console.log('clearStateState error', e.message);
    });
  }

  getUser() {
    this.mgr.getUser().then((user) => {
      this.currentUser = user;
      console.log('got user', user);
      this.userLoadededEvent.emit(user);
    }).catch(function (err) {
      console.log(err);
    });
  }

  removeUser() {
    this.mgr.removeUser().then(() => {
      this.userLoadededEvent.emit(null);
      console.log('user removed');
    }).catch(function (err) {
      console.log(err);
    });
  } 

  startSignoutMainWindow() {
    this.mgr.getUser().then(user => {
      return this.mgr.signoutRedirect({ "id_token_hint": user.id_token }).then(resp => {
        
        console.log('signed out', resp);
        setTimeout(5000, () => {
          console.log('testing to see if fired...');
        });
      }).catch(function (err) {
        console.log(err);
      });
    });   
  };

  endSignoutMainWindow() {
    this.mgr.signoutRedirectCallback().then(function (resp) {
      console.log('signed out', resp);
    }).catch(function (err) {
      console.log("signout not success");
      console.log(err);
    });
  };
  /**
   * Example of how you can make auth request using angulars http methods.
   * @param options if options are not supplied the default content type is application/json
   */
  AuthGet(url: string, options?: RequestOptions): Observable<Response> {
    if (options) {
      options = this._setRequestOptions(options);
    }
    else {
      options = this._setRequestOptions();
    }
    return this.http.get(url, options);
  }
  /**
   * @param options if options are not supplied the default content type is application/json
   */
  AuthPut(url: string, data: any, options?: RequestOptions): Observable<Response> {

    let body = JSON.stringify(data);

    if (options) {
      options = this._setRequestOptions(options);
    }
    else {
      options = this._setRequestOptions();
    }
    return this.http.put(url, body, options);
  }
  /**
   * @param options if options are not supplied the default content type is application/json
   */
  AuthDelete(url: string, options?: RequestOptions): Observable<Response> {

    if (options) {
      options = this._setRequestOptions(options);
    }
    else {
      options = this._setRequestOptions();
    }
    return this.http.delete(url, options);
  }
  /**
   * @param options if options are not supplied the default content type is application/json
   */
  AuthPost(url: string, data: any, options?: RequestOptions): Observable<Response> {

    let body = JSON.stringify(data);

    if (options) {
      options = this._setRequestOptions(options);
    } else {
      options = this._setRequestOptions();
    }
    return this.http.post(url, body, options);
  }


  private _setAuthHeaders(user: any): void {
    this.authHeaders = new Headers();
    this.authHeaders.append('Authorization', user.token_type + ' ' + user.access_token);
    this.authHeaders.append('Access-Control-Allow-Origin', '*');
    this.authHeaders.append('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');

    if (this.authHeaders.get('Content-Type')) {

    } else {
      this.authHeaders.append('Content-Type', 'application/json');
    }
  }
  private _setRequestOptions(options?: RequestOptions) {
    if (this.loggedIn) {
      this._setAuthHeaders(this.currentUser);
    }
    if (options) {
      options.headers.append(this.authHeaders.keys[0], this.authHeaders.values[0]);
    } else {
      options = new RequestOptions({ headers: this.authHeaders });
    }

    return options;
  }

}


