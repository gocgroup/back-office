import { Action } from '@ngrx/store';

export const ADD_USER = '[ADD_USER] Add';
export const REMOVE_USER = '[REMOVE_USER] Remove';


export class AddUser implements Action {
    readonly type = ADD_USER;
    constructor(public user: string){
    }
}
export class RemoveUser implements Action {
    readonly type = REMOVE_USER;
    constructor(public user: string){
    }
}