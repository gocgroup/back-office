import { Action } from '@ngrx/store';
export const USER_LOGGEDIN = '[USER_LOGGEDIN] UserLoggedIn';
export const ADDED_COMPANY = '[ADDED_COMPANY] Added';
export const EDITED_COMPANY = '[EDITED_COMPANY] Edited';


export class AddedCompany implements Action {
    readonly type = ADDED_COMPANY;
    constructor(public id: string, public name: string, public loggedUser: string){
    }
}
