import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SalesBarChartService } from './sales-bar-chart/sales-bar-chart.service';
import { ProfitPieChartService } from './profit-pie-chart/profit-pie-chart.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule} from '@angular/material/grid-list';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { NavFooterComponent } from './nav-footer/nav-footer.component';
import { HomeComponent } from './home/home.component';
import { CompanyComponent } from './company/company.component';
import { SalesBarChartComponent } from './sales-bar-chart/sales-bar-chart.component';
import { ProfitPieChartComponent } from './profit-pie-chart/profit-pie-chart.component';
import { CardActionBarComponent } from './card-action-bar/card-action-bar.component';
import { MatIconRegistry, MatIconModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { AppRoutes, AuthProviders } from './app.routing';
import { HttpModule } from '@angular/http';
import { userReducer } from './reducers/user.reducer';
import { StoreModule } from '@ngrx/store';
import { AuthSignoutCallbackComponent } from './auth/auth-signout-callback/auth-signout-callback.component';
import { AuthCallbackComponent } from './auth/auth-callback/auth-callback.component';
import { ToastrModule } from 'ngx-toastr';
import { FocusDirective } from './directives/focus.directive'
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { InventoryHomeComponent } from './inventory-home/inventory-home.component';
import { CompanyCardComponent } from './company-card/company-card.component';
import { CompanyService } from './services/company.service';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    NavFooterComponent,
    CompanyComponent,
    CardActionBarComponent,
    SalesBarChartComponent,
    ProfitPieChartComponent,
    AuthCallbackComponent,
    AuthSignoutCallbackComponent,
    FocusDirective,
    InventoryHomeComponent,
    CompanyCardComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MatButtonModule,
    MatGridListModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule,
    MatToolbarModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatListModule,
    HttpModule,
    HttpClientModule,
    MatSidenavModule,
    HttpClientModule, 
    FormsModule,
    MatDividerModule,
    AppRoutes,
    AngularFontAwesomeModule,
    StoreModule.forRoot({user:userReducer}),
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    ToastrModule.forRoot()

  ],
  providers: [
  SalesBarChartService, 
  ProfitPieChartService, 
  AuthProviders,
  CompanyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
