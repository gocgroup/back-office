import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[focus]'
})
export class FocusDirective {
    constructor(private _el: ElementRef, private renderer: Renderer) {
    }

    ngAfterViewInit() {
        this.renderer.invokeElementMethod(this._el.nativeElement, 'focus');
    }
}