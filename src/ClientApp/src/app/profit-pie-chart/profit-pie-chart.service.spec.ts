import { TestBed, inject } from '@angular/core/testing';

import { ProfitPieChartService } from './profit-pie-chart.service';

describe('ProfitPieChartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfitPieChartService]
    });
  });

  it('should be created', inject([ProfitPieChartService], (service: ProfitPieChartService) => {
    expect(service).toBeTruthy();
  }));
});
