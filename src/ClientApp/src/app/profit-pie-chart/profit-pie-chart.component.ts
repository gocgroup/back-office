﻿import { Component, Injectable } from '@angular/core';
import { ProfitPieChartService } from './profit-pie-chart.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-profit-pie-chart',
  templateUrl: './profit-pie-chart.component.html',
  styleUrls: ['./profit-pie-chart.component.css']

})
@Injectable()
export class ProfitPieChartComponent {

  chart = []; // This will hold our chart info

  constructor(private _service: ProfitPieChartService) {}

  ngOnInit() {
    this._service.getPieChartData()
      .subscribe(res => {
         let data = {
            datasets: [{
                data: [10, 20, 30],
                backgroundColor: [
                    "#FF6384",
                  "#36A2EB",
                  "#FFCE56"
                ],
            }],
            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Red',
                'Yellow',
                'Blue'
            ]
        };
        //TODO replace fake data with data coming from service
        this.chart = new Chart('pieChartCanvas', {
            type: 'pie',
            data: data,
            options: {}
        });
      })
  }

}