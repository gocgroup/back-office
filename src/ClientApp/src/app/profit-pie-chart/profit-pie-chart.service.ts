import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ProfitPieChartService {

 constructor(private _http: HttpClient) { }

  getPieChartData() {
    return this._http.get("http://api.openweathermap.org/data/2.5/weather?q=London&appid=7dad588d82f8da6ad72633e8e8975623")
      .map(result => result);
   
  }
}