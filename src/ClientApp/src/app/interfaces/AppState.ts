import{ LoggedInUser} from '../interfaces/LoggedInUser'

export interface AppState{
    loggedInUser: LoggedInUser;
    companyId: string;
  }