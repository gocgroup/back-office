export interface Company{
    id: string;
    name: string;
    phoneNumber: string;
  }