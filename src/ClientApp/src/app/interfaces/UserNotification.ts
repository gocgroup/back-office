export interface UserNotification{
    from: string;
    body: string;
  }