import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthService } from './auth/auth.service';
import { AuthGuardService } from './auth/auth-guard.service';
import { AuthCallbackComponent } from './auth/auth-callback/auth-callback.component';
import { AuthSignoutCallbackComponent } from './auth/auth-signout-callback/auth-signout-callback.component';
import { InventoryHomeComponent } from './inventory-home/inventory-home.component';


const appRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/home',
    },
    {
        path: 'home',
        pathMatch: 'full',
        component: HomeComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'inventory',
        pathMatch: 'full',
        component: InventoryHomeComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'auth-callback',
        component: AuthCallbackComponent
     },
    {
        path: 'auth-signout-callback',
        component: AuthSignoutCallbackComponent  
    }
];
export const AuthProviders = [
    AuthGuardService,
    AuthService
];

export const AppRoutes = RouterModule.forRoot(appRoutes);

