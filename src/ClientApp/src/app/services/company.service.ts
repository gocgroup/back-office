import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import 'rxjs/add/operator/map';

@Injectable()
export class CompanyService {

  constructor(private _authService: AuthService) { }
  
  getCompanyById() {
    //TODO replace this url with appropiate url to get char data
     return this._authService.AuthGet("http://vagrant:5001/api/companies/96332d81-bad1-4332-b243-60f01ef46969")
       .map(result => result);
   }

   createCompany() {
     var company = {
      Name : "GOC",
      UserId : 123,
      PhoneNumber : "2252332234",
      Address : 
       {
           AddressLine1 : "123 Elm Street St",
           City: "Los Angeles",
           State: "California",
           ZipCode: "34322"
       }
   };
    //TODO replace this url with appropiate url to get char data
     return this._authService.AuthPost("http://vagrant:5001/api/companies/", company)
       .map(result => result);
   }
}
