﻿import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-action-bar',
  templateUrl: './card-action-bar.component.html',
  styleUrls: ['./card-action-bar.component.css']

})
export class CardActionBarComponent {
@Input() cardId: number;

ngOnInit() {
  }
}
