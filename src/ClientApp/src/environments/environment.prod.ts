export const environment = {
  production: true,
  identity:{
    authority: "https://dev.gocgroup.net/identity",
    clientId: "goc-backoffice",
    redirectUri : "https://dev.gocgroup.net/auth-callback",
    logoutRedirectUri: "https://dev.gocgroup.net/auth-signout-callback",
    responseType: "id_token token",
    scope: "openid api1"
    // authority: "http://vagrant/identity",
    // clientId: "goc-backoffice",
    // redirectUri : "http://vagrant/auth-callback",
    // logoutRedirectUri: "http://vagrant/auth-signout-callback",
    // responseType: "id_token token",
    // scope: "openid api1"
 },
 baseUrl: "http://vagrant/"
};
