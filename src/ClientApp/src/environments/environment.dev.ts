export const environment = {
    production: false,
    identity:{
        authority: "http://localhost:5000/identity",
        clientId: "goc-backoffice",
        redirectUri : "http://localhost:5004/auth-callback",
        logoutRedirectUri: "http://localhost:5004/auth-signout-callback",
        responseType: "id_token token",
        scope: "openid api1"
     },
     baseUrl: "htpp://localhost:5004/"
  };

