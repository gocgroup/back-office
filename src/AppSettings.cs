﻿namespace GOC.BackOffice
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class AppSettings
    {
        public IdentitySettings Identity { get; set; }
    }

    public class IdentitySettings
    {
        public string Authority { get; set; }

        public string ClientSecret { get; set; }
        public string ClientId { get; set; }
        public int AccessTokenExpirationSkewInSeconds { get; set; }
    }
}
